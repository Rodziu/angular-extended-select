# AngularJS extended select component.

[Live Demo](https://mateuszrohde.pl/repository/angular-extended-select/demo/index.html)

Extended HTML select component, with ability to easily search possible options and better presentation with use of [Twitter Bootstrap](https://getbootstrap.com/docs/3.3/).

## Prerequisites

- AngularJS,
- Twitter Bootstrap.

## Installation

```
yarn add angular-extended-select
yarn install
```

## Features

- Search options, 
	- navigate through them using arrow keys,
	- resolve options from external source during search,
	- hide options until some characters are typed,
	- add options on the fly,
- deselect options,
- fits nicely with Bootstrap input-sm & input-lg classes.
 
See more at [Live Demo](https://mateuszrohde.pl/repository/angular-extended-select/demo/index.html).
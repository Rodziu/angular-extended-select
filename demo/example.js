/*
 * Angular extended select element plugin for AngularJS.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const app = angular.module('exampleApp', ['angularExtendedSelect']);
	app.controller('exampleCtrl', ['$scope', '$http', '$q', function($scope, $http, $q){
		$scope.options = [];
		$scope.shortOptions = [];
		$http.get('mock_data.json').then(function(response){
			$scope.options = response.data;
			$scope.shortOptions = response.data.slice(0, 100);
		});

		$scope.resolvedOptions = [];
		$scope.resolveOnSearch = function(search){
			search = search.toLowerCase();
			const defered = $q.defer(),
				results = [];
			for(let i = 0; i < $scope.options.length; i++){
				if(!!~$scope.options[i].word.indexOf(search)){
					results.push($scope.options[i]);
					if(results.length === 10){
						break;
					}
				}
			}
			$scope.resolvedOptions = results;
			defered.resolve();
			return defered.promise;
		};

		$scope.emptyOptions = [];
		$scope.addOptionCallback = function(newOption){
			$scope.emptyOptions.push({
				id: -1,
				word: newOption
			});
		};

		$scope.multiple = [];
	}]);
	/**
	 * @ngdoc component
	 * @name codeExample
	 */
	app.component('codeExample', {
		transclude: true,
		bindings: {
			html: '<'
		},
		controllerAs: 'ctrl',
		template: '<p>Code:</p><code ng-repeat="h in ctrl.html">{{h}}</code>',
		controller: [function(){
			const ctrl = this;
			ctrl.$onInit = function(){
				ctrl.html = ctrl.html.split("\n");
			};
		}]
	});
	/**
	 * @ngdoc directive
	 * @name codeExampleHook
	 */
	app.directive('codeExampleHook', ['$compile', function($compile){
		return {
			restrict: 'A',
			priority: 999999999999,
			compile: function(element){
				const html = element.clone().removeAttr('code-example-hook')[0].outerHTML;
				return function(scope){
					const newScope = scope.$new(),
						newElement = angular.element('<code-example html="html"></code-example>');
					newScope.html = html;
					element.after(newElement);
					$compile(newElement)(newScope);
				}
			}
		};
	}]);
}();
/*
 * Angular extended select element plugin for AngularJS.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const app = angular.module('angularExtendedSelect', ['angularBS']);
	/**
	 * Config
	 */
	app.provider("extendedSelect", function(){
		this.options = {
			placeholder: '\u00A0',
			placeholderMultiple: '\u00A0',
			typeToSearch: 0,
			typeToSearchText: 'Zacznij pisać, by wyświetlić dostępne opcje',
			addOptionLang: 'Dodaj',
			searchByValue: false
		};
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * Prevent ng-options directive from compiling on angular-extended-select
	 */
	app.config(['$provide', function($provide){
		const blocked = ['ngOptions', 'select'];
		angular.forEach(blocked, function(d){
			$provide.decorator(d + 'Directive', ['$delegate', function($delegate){
				angular.forEach($delegate, function(directive){
					const compile_ = directive.compile || function(){
					};
					directive.compile = function(element){
						if(element[0].tagName === 'EXTENDED-SELECT'){
							return;
						}
						return compile_.apply(this, arguments) || directive.link;
					};
				});
				return $delegate;
			}]);
		});
	}]);
	/**
	 * @ngdoc factory
	 * @name ExtendedSelectOptions
	 * @description extended-select helper functions
	 */
	app.factory('ExtendedSelectOptions', ['$parse', function($parse){
		/**
		 * Parse ng-options in angular way.
		 * 1: value expression (valueFn)
		 * 2: label expression (displayFn)
		 * 3: group by expression (groupByFn)
		 * 4: disable when expression (disableWhenFn)
		 * 5: array item variable name
		 * 6: object item key variable name
		 * 7: object item value variable name
		 * 8: collection expression
		 * 9: track by expression
		 * @type {RegExp}
		 */
		return {
			NG_OPTIONS_REGEXP: /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?(?:\s+disable\s+when\s+([\s\S]+?))?\s+for\s+(?:([$\w][$\w]*)|(?:\(\s*([$\w][$\w]*)\s*,\s*([$\w][$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,
			parseNgOptions: function(ngOptionsString){
				const match = ngOptionsString.match(this.NG_OPTIONS_REGEXP);
				if(match == null){
					return null;
				}
				const valueName = match[5] || match[7],
					keyName = match[6];
				return {
					valueFn: $parse(match[2] ? match[1] : valueName),
					displayFn: $parse(match[2] || match[1]),
					valuesFn: $parse(match[8]),
					getLocals: function(k, v){
						const locals = {};
						locals[valueName] = v;
						if(keyName){
							locals[keyName] = k;
						}
						return locals;
					}
				};
			}
		};
	}]);
	const extendedSelectComponent = {
		require: {
			ngModelCtrl: 'ngModel'
		},
		bindings: {
			ngModel: '=',
			addOption: '<?',
			resolveOnSearch: '<?',
			deselectable: '<?',
			typeToSearch: '<?',
			searchByValue: '<?'
		},
		transclude: true,
		templateUrl: ['$attrs', function($attrs){
			if('multiple' in $attrs){
				return "src/templates/extended-select-multiple.ng";
			}
			return "src/templates/extended-select.ng";
		}],
		controllerAs: 'ctrl',
		controller: [
			'$element', '$attrs', '$scope', '$timeout', 'ExtendedSelectOptions', 'extendedSelect',
			function($element, $attrs, $scope, $timeout, ExtendedSelectOptions, extendedSelect){
				const ctrl = this,
					transcludeElement = $element[0].querySelector('[ng-transclude]'),
					ngOptions = 'ngOptions' in $attrs
						? ExtendedSelectOptions.parseNgOptions($attrs['ngOptions']) : null,
					updateMultipleModel = function(newValue, removeValue){
						// sort selected options, so we get same result as in select element.
						const sorted = [];
						for(let i = 0; i < ctrl.options.length; i++){
							if(
								(
									ctrl.isSelected(ctrl.options[i])
									|| angular.equals(ctrl.options[i].value, newValue)
								)
								&& !angular.equals(ctrl.options[i].value, removeValue)
							){
								sorted.push(ctrl.options[i].value);
							}
						}
						if(!angular.equals(ctrl.ngModel, sorted)){
							ctrl.ngModel = sorted;
						}
					};
				/**
				 * @returns {*}
				 */
				ctrl.getModelValue = function(value){
					if(typeof value === 'undefined'){
						value = ctrl.ngModel;
					}
					for(let o = 0; o < ctrl.options.length; o++){
						if(angular.equals(ctrl.options[o].value, value)){
							return ctrl.options[o].label;
						}
					}
					return false;
				};
				let searchTimeout = null,
					lastSearchValue;
				/**
				 * Search change callback
				 */
				ctrl.searchFn = function(){
					ctrl.activeIndex = ctrl.options.length ? 0 : -1;
					if(typeof ctrl.resolveOnSearch === 'function' && !ctrl.multiple){
						if(typeof ctrl.search !== 'undefined' && ctrl.search.length){
							const timeout = typeof lastSearchValue === 'undefined' ? 0 : 750;
							if(searchTimeout !== null){
								$timeout.cancel(searchTimeout);
							}
							lastSearchValue = ctrl.search;
							ctrl.loading = true;
							searchTimeout = $timeout(function(){
								searchTimeout = null;
								ctrl.resolveOnSearch(ctrl.search).then(function(){
									lastSearchValue = undefined;
									ctrl.loading = false;
								}).catch(function(){
								});
							}, timeout);
						}
					}
				};
				/**
				 * @param option
				 */
				ctrl.pickOption = function(option){
					if(ctrl.multiple){
						if(typeof ctrl.ngModel == 'undefined'){
							ctrl.ngModel = [];
						}
						if(!~ctrl.ngModel.indexOf(option.value)){
							updateMultipleModel(option.value);
						}
						ctrl.activeIndex = -1;
						ctrl.search = '';
					}else{
						ctrl.isOpen = false;
						ctrl.ngModel = option.value;
						ctrl.activeIndex = ctrl.options.indexOf(option);
					}
				};
				/**
				 * @param option
				 * @returns {boolean}
				 */
				ctrl.isSelected = function(option){
					if(typeof ctrl.ngModel === 'undefined'){
						return false;
					}
					if(ctrl.multiple){
						return !!~ctrl.ngModel.indexOf(option.value);
					}
					return angular.equals(option.value, ctrl.ngModel);
				};
				/**
				 * @param value
				 */
				ctrl.deselect = function(value){
					if(ctrl.multiple){
						updateMultipleModel(undefined, value);
					}else{
						ctrl.ngModel = undefined;
					}
					ctrl.activeIndex = -1;
				};
				/**
				 */
				ctrl.addOptionAction = function(){
					if(typeof ctrl.addOption === 'function' && ctrl.search.length){
						let found = false;
						for(let o = 0; o < ctrl.options.length; o++){
							if(ctrl.options[o].label === ctrl.search){
								ctrl.pickOption(ctrl.options[o]);
								found = true;
								break;
							}
						}
						if(!found){
							ctrl.addOption(ctrl.search);
							ctrl.addOptionCalled = true;
							// we set this flag, so we can update ngModel with proper option,
							// which will be generated on next digest cycle
						}
					}
				};
				/**
				 */
				$element.on('click', function(){
					const wasOpen = ctrl.isOpen;
					ctrl.ngModelCtrl.$setTouched();
					if($attrs.disabled){
						ctrl.isOpen = false;
					}else{
						ctrl.isOpen = ctrl.multiple ? true : !ctrl.isOpen;
					}
					if(!wasOpen && ctrl.isOpen){
						ctrl.search = '';
						// reset active index
						ctrl.activeIndex = -1;
						for(let i = 0; i < ctrl.options.length; i++){
							if(ctrl.isSelected(ctrl.options[i])){
								ctrl.activeIndex = i;
								if(!ctrl.multiple){
									break;
								}
							}
						}
					}
					$scope.$digest();
					if(!wasOpen && ctrl.isOpen){
						ctrl.searchElement[0].focus();
					}
				});
				/**
				 */
				$attrs.$observe('disabled', function(value){
					ctrl.isDisabled = value;
				});
				/**
				 */
				$attrs.$observe('placeholder', function(value){
					ctrl.placeholder = value;
				});
				/**
				 * Init
				 */
				ctrl.$onInit = function(){
					ctrl.options = [];
					ctrl.optionsFiltered = [];
					ctrl.activeIndex = -1;
					ctrl.search = '';
					ctrl.multiple = 'multiple' in $attrs;
					ctrl.deselectable = 'deselectable' in $attrs;
					ctrl.addOptionLang = extendedSelect.addOptionLang;
					if(typeof ctrl.typeToSearch === 'undefined'){
						ctrl.typeToSearch = extendedSelect.typeToSearch;
					}
					ctrl.typeToSearchText = extendedSelect.typeToSearchText;
					if(typeof ctrl.searchByValue === 'undefined'){
						ctrl.searchByValue = extendedSelect.searchByValue;
					}
					if(typeof ctrl.placeholder === 'undefined'){
						ctrl.placeholder = 'multiple' in $attrs
							? extendedSelect.placeholderMultiple : extendedSelect.placeholder
					}
					if($element.hasClass('input-sm')){
						$element.children().addClass('input-sm');
						$element.removeClass('input-sm');
					}
					if(ctrl.multiple){
						/**
						 * @var ngModelCtrl
						 * @type {{}}
						 */
						ctrl.ngModelCtrl.$isEmpty = function(value){
							return !value || value.length === 0;
						};
					}
				};
				/**
				 * Check
				 */
				ctrl.$doCheck = function(){
					const optionElements = transcludeElement.querySelectorAll('option'),
						options = [];
					let pickLater;
					for(let i = 0; i < optionElements.length; i++){
						// noinspection JSCheckFunctionSignatures
						const option = angular.element(optionElements[i]);
						options.push({
							value: option.val(),
							label: option.text().trim()
						});
					}
					if(ngOptions !== null){
						const optionObjects = ngOptions.valuesFn($scope.$parent);
						for(let i = 0; i < optionObjects.length; i++){
							if(ngOptions === null){
								options.push({
									value: optionObjects[i],
									label: optionObjects[i]
								});
							}else{
								const locals = ngOptions.getLocals(i, optionObjects[i]);
								options.push({
									value: ngOptions.valueFn(locals),
									label: ngOptions.displayFn(locals)
								});
							}
							if(ctrl.addOptionCalled && options[options.length - 1].label === ctrl.search){
								pickLater = options[options.length - 1];
								ctrl.addOptionCalled = false;
							}
						}
					}
					if(!angular.equals(options, ctrl.options)){
						ctrl.options = options;
						if(typeof pickLater !== 'undefined'){
							// in multiple mode, we need to wait until new option is added to ctrl.options
							// before selecting it
							ctrl.pickOption(pickLater);
						}
					}
					ctrl.isSmall = $element.hasClass('input-sm');
					ctrl.isLarge = $element.hasClass('input-lg');
				};
			}
		]
	};
	/**
	 * @ngdoc component
	 * @name extendedSelect
	 *
	 * @param {expression} ngModel
	 * @param {expression|function} addOption
	 * @param {expression|function} resolveOnSearch
	 * @param {string} deselectable
	 * @param {expression|number} typeToSearch
	 * @param {expression|boolean} searchByValue
	 * @param {String} placeholder
	 * @parma {String} multiple
	 */
	app.component('extendedSelect', extendedSelectComponent);
	/**
	 * @ngdoc directive
	 * @name extendedSelectDropdown
	 */
	app.directive('extendedSelectDropdown', [function(){
		return {
			restrict: 'A',
			link: function(scope, element){
				element.on('click', function(e){
					e.stopPropagation(); // prevent dropdown from closing
				});
			}
		}
	}]);
	/**
	 * @ngdoc directive
	 * @name extendedSelectSearch
	 * @description search element
	 */
	app.directive('extendedSelectSearch', [function(){
		return {
			restrict: 'A',
			require: '^extendedSelect',
			link: function(scope, element, attrs, ctrl){
				ctrl.searchElement = element;
				/**
				 * move selection or pick an option on keydown
				 */
				element.on('keydown', function(e){
					if(!ctrl.optionsFiltered.length){
						if(e.which == 13){
							ctrl.addOptionAction();
							scope.$apply();
						}
						return;
					}
					const originalIndex = ctrl.activeIndex;
					switch(e.which){
						case 40: // down
							do{
								ctrl.activeIndex++;
								if(ctrl.activeIndex >= ctrl.optionsFiltered.length){
									ctrl.activeIndex = originalIndex;
									break;
								}
							}while(ctrl.multiple && ctrl.isSelected(ctrl.optionsFiltered[ctrl.activeIndex]));
							break;
						case 38: // up
							do{
								ctrl.activeIndex--;
								if(ctrl.activeIndex < 0){
									ctrl.activeIndex = originalIndex;
									break;
								}
							}while(ctrl.multiple && ctrl.isSelected(ctrl.optionsFiltered[ctrl.activeIndex]));
							break;
						case 13: // enter
							if(typeof ctrl.optionsFiltered[ctrl.activeIndex] !== 'undefined'){
								ctrl.pickOption(ctrl.optionsFiltered[ctrl.activeIndex]);
								scope.$apply();
								return;
							}
							break;
					}
					scope.$digest();
				});
			}
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name extendedSelectOptions
	 * @description automatically scroll dropdown window to highlighted option
	 */
	app.directive('extendedSelectOptions', [function(){
		return {
			restrict: 'A',
			bindToController: {
				activeIndex: '<extendedSelectOptions'
			},
			controller: ['$element', 'angularBS', function($element, angularBS){
				const ctrl = this;
				ctrl.$onChanges = function(){ // it's always an activeIndex change
					const li = $element[0].querySelector(`li:nth-child(${ctrl.activeIndex + 1})`);
					if(li === null){
						return;
					}
					const top = li.offsetTop,
						scroll = $element[0].scrollTop,
						bot = angularBS.offset(li).height + top,
						ulHeight = angularBS.offset($element[0]).height;
					if(scroll - top > 0){ // move it up
						$element[0].scrollTop = top;
					}else if(scroll - bot < ulHeight * -1){ // move it down
						$element[0].scrollTop = bot - ulHeight;
					}
				};
			}]
		};
	}]);
	/**
	 * @ngdoc filter
	 * @name extendedSelectFilter
	 * @description Filter visible options
	 */
	app.filter('extendedSelectFilter', [function(){
		return function(options, search, typeToSearch, searchByValue){
			if(typeof search === 'undefined'){
				return options;
			}
			if(search.length < typeToSearch){
				return [];
			}
			const ret = [];
			search = search.toLowerCase();
			for(let o = 0; o < options.length; o++){
				if(
					!!~options[o].label.toLowerCase().indexOf(search)
					|| (searchByValue && !!~options[o].value.toLowerCase().indexOf(search))
				){
					ret.push(options[o]);
				}
			}
			return ret;
		};
	}]);
	/**
	 * @ngdoc filter
	 * @name extendedSelectSearch
	 * @description Underscore results in dropdown
	 */
	app.filter('extendedSelectSearch', ['$sce', function($sce){
		return function(input, search){
			if(search.length){
				input = input.replace(new RegExp('(' + search + ')', 'gi'), '<u>$1</u>');
			}
			//noinspection JSUnresolvedFunction
			return $sce.trustAsHtml(input);
		};
	}]);
}();